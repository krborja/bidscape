from django.apps import AppConfig


class ProductbidsConfig(AppConfig):
    name = 'productbids'
