from django.db import models

# Create your models here.

class Product(models.Model):
  name = models.CharField(max_length=50)
  description = models.TextField()
  min_bid_amount = models.FloatField()
  max_bid_amount = models.FloatField()
  expiry_date = models.DateTimeField()
  is_active = models.BooleanField(default=True)
  created_at = models.DateTimeField(auto_now_add=True)