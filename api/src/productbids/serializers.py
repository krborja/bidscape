from rest_framework import serializers
from .models import Product

# Product Serializer
class ProductSerializer(serializers.ModelSerializer):
  class Meta:
    model = Product
    fields = [
      'id', 
      'name',
      'description', 
      'min_bid_amount', 
      'max_bid_amount', 
      'expiry_date',
      'is_active',
      'created_at'
    ]