from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

# Create your models here.

class UserAccountManager(BaseUserManager): # overriding the base user manager
  def create_user(self, email, first_name, last_name, role, password):
    if not email:
      raise ValueError('Users must have an email address')
  
    email = self.normalize_email(email)  # lowercasing the domain portion
    user = self.model(
      email=email, 
      first_name=first_name, 
      last_name=last_name, 
      role=role
    )

    user.set_password(password) # will create a hashed version of the password
    user.save()

    return user

  # def create_superuser()



class UserAccount(AbstractBaseUser, PermissionsMixin):
  email = models.EmailField(max_length=100, unique=True)
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  is_active = models.BooleanField(default=True)
  is_staff = models.BooleanField(default=True)
  role = models.CharField(max_length=50) # SELLER or BUYER

  objects = UserAccountManager()

  USERNAME_FIELD = 'email' # use email for default login instead of username
  REQUIRED_FIELDS = ['first_name', 'last_name', 'role'] 


  def __str__(self):
    return self.email