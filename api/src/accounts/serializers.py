from djoser.serializers import UserCreateSerializer
# If we have a custom user model, this is how we retrieve it
from django.contrib.auth import get_user_model
User = get_user_model()

class UserCreateSerializer(UserCreateSerializer):
  class Meta(UserCreateSerializer.Meta):
    model = User
    field = (
      'id', 
      'email', 
      'first_name', 
      'last_name', 
      'role', 
      'password'
    )